## Note on Fastapi with JWT

https://testdriven.io/blog/fastapi-jwt-auth/

https://github.com/testdrivenio/fastapi-jwt/tree/main/app

# After running the main app

access: http://localhost:8081/docs

## API Documentation

To view your API documentation, go to:

http://localhost:8081/redoc

# Generate access token

```
export CURL_OPTS="--data-urlencode username=user --data-urlencode password=pass"

curl -H "Content-Type: application/x-www-form-urlencoded" \
    -d "client_id=confi" \
    -d "grant_type=password" \
    ${CURL_OPTS} http://localhost:8080/realms/softcomweb/protocol/openid-connect/token | jq -r  '.access_token'
```

# Realm client settings

http://localhost:8080/admin/realms/softcomweb/clients/confi
# Another commit to test commit signing
# Another commit to test commit signing
