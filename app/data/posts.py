from typing import List, Dict, Any

def get_posts() -> List[Dict[str, Any]]:
    """Return a list of Posts
    """

    posts: List[Dict[str, Any]] = [
        {
            "id": 1,
            "title": "One",
            "content": "First object"
        },
        {
            "id": 2,
            "title": "Two",
            "content": "Second object"
        }
    ]

    return posts