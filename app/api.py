from fastapi import FastAPI, Body, Depends
from app.model import PostSchema
from typing import List, Dict, Any
from app.data import posts
from app.auth.auth_bearer import JWTBearer

app = FastAPI()

@app.get("/", tags=["root"])
async def get_root() -> dict:
    return {
        "message": "Welcome to softcomweb",
        "status": "OK"
    }



users = [PostSchema(**p) for p in posts.get_posts()]

@app.get("/posts", dependencies=[Depends(JWTBearer(roles=["developer"]))], tags=["posts"])
async def get_posts() -> dict:
    return {
        "data": users
    }

@app.get("/posts/{id}", tags=["posts"])
async def get_single_post(id: int) -> dict:
    if id > len(users):
        return {
            "error": "ID not found"
        }
    result = list(filter(lambda x: x.id == id, users))[0]
    return result

@app.post("/posts", dependencies=[Depends(JWTBearer(roles=["admin"]))], tags=["posts"])
async def add_post(post: PostSchema) -> dict:
    post.id = len(users) + 1
    users.append(PostSchema(**post.dict()))
    return {
        "data": "post added"
    }