from fastapi import Request, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials

from .auth_decoder import decode_token

class JWTBearer(HTTPBearer):
    def __init__(self, auto_error: bool = True, roles = []):
        super(JWTBearer, self).__init__(auto_error=auto_error)
        self.roles = roles

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(
                    status_code=403,
                    detail="invalid authentication schema"
                )
            if not self.verify_jwt(credentials.credentials):
                raise HTTPException(
                    status_code=403,
                    detail="Invalid token or expired token"
                )
            return credentials.credentials
        else:
            raise HTTPException(
                status_code=403,
                detail="Invalid Authorization code"
            )
    def verify_jwt(self, jwtoken: str) -> bool:
        isTokenValid: bool = False
        try:
            decoded_token = decode_token(jwtoken)
            print(decoded_token)
            if self.roles:
                for role in self.roles:
                    if role in decoded_token['roles']:
                        isTokenValid = True
        except Exception as ex:
            raise Exception(f"Could not verify token: {ex}")
        
        return isTokenValid