import json
import time

import jwt
import requests
from jwt.algorithms import RSAAlgorithm

PUBLIC_KEY = None

class UnauthorizedError(Exception):
    pass

def get_public_key() -> any:
    global PUBLIC_KEY

    if PUBLIC_KEY:
        return PUBLIC_KEY

    config = requests.get(
        "http://localhost:8080/realms/softcomweb/.well-known/openid-configuration"
    ).json()
    keys = requests.get(config["jwks_uri"]).json()
    PUBLIC_KEY = RSAAlgorithm.from_jwk(json.dumps(keys["keys"][1]))
    return PUBLIC_KEY


def get_token(auth_header: str) -> list:
    if not auth_header:
        raise UnauthorizedError("Authorization header missing")

    auth_header_parts = [x for x in auth_header.split(" ") if x]



    if len(auth_header_parts) != 2 or auth_header_parts[0] not in ["bearer", "Bearer"]:
        raise UnauthorizedError("Malformed authorization header. Expected '<type> <token>'")

    return auth_header_parts

def decode_token(bearer_token: str) -> dict:
    try:

        decoded_token = jwt.decode(bearer_token, get_public_key(), algorithms="RS256", verify=False)
        return decoded_token if decoded_token["exp"] >= time.time() else None
    except Exception as e:
        raise UnauthorizedError(f"Invalid token: {e}")

