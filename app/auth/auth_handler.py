from typing import Dict
import requests
import jwt
from datetime import datetime

class Authorizer:
    def __init__(self, client_id: str, client_secret: str, token_uri: str):
        self._client_id = client_id
        self._client_secret = client_secret
        self._token_uri = token_uri
        self._token = None
        self._expiration_time = 0

    def token(self) -> str:
        now = int(datetime.now().timestamp())
        if not self._token or now >= self._expiration_time:
            response = requests.post(
                self._token_uri,
                data={
                    "grant_type": "client_credentials",
                    "client_id": self._client_id,
                    "client_secret": self._client_secret,
                },
            )
            response.raise_for_status()
            self._token = response.json().get("access_token")
            if self._token:
                decoded = jwt.decode(self._token, options={"verify_signature": False})
                self._expiration_time = decoded.get("exp", 0)
        return self._token or "None"

    def __call__(self) -> tuple[str, str]:
        return "Bearer", self.token()